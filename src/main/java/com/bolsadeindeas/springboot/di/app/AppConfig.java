package com.bolsadeindeas.springboot.di.app;

import com.bolsadeindeas.springboot.di.app.model.domain.ItemFactura;
import com.bolsadeindeas.springboot.di.app.model.domain.Producto;
import com.bolsadeindeas.springboot.di.app.model.service.IServicio;
import com.bolsadeindeas.springboot.di.app.model.service.MiServicio;
import com.bolsadeindeas.springboot.di.app.model.service.MiServicioComplejo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

@Configuration
public class AppConfig {
    @Bean("miServicioSimple")
    public IServicio registrarMiServicio(){
        return new MiServicio();
    }

    @Bean("miServicioComplejo")
    public IServicio registrarMiComplejo(){
        return new MiServicioComplejo();
    }

    @Bean("itemsFactura")
    public List <ItemFactura> registrarItems(){
        Producto producto1 = new Producto("Camara Sonny",100);
        Producto producto2 = new Producto("Motocicleta",21000);

        ItemFactura linea1 = new ItemFactura(producto1,2);
        ItemFactura linea2 = new ItemFactura(producto2,4);

        return Arrays.asList(linea1,linea2);
    }
    @Bean("itemsFacturaOficina")
    public List <ItemFactura> registrarItemsOficina(){
        Producto producto1 = new Producto("Monitor LG",290);
        Producto producto2 = new Producto("Teclado Iyani",21);
        Producto producto3 = new Producto("Mouse Gforce",60);
        Producto producto4 = new Producto("Impresora HP",1000);

        ItemFactura linea1 = new ItemFactura(producto1,2);
        ItemFactura linea2 = new ItemFactura(producto2,4);
        ItemFactura linea3 = new ItemFactura(producto3,6);
        ItemFactura linea4 = new ItemFactura(producto4,1);

        return Arrays.asList(linea1,linea2,linea3,linea4);
    }
}
