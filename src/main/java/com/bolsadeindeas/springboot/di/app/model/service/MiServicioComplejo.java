package com.bolsadeindeas.springboot.di.app.model.service;

import org.springframework.stereotype.Component;

//@Component("miServicioComplejo")
public class MiServicioComplejo implements IServicio{
    @Override
    public String operacion(){
        return "Ejecutando algún proceso importante complejo...";
    }
}
