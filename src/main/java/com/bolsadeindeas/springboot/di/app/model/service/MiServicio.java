package com.bolsadeindeas.springboot.di.app.model.service;

import org.springframework.stereotype.Component;

//@Component("miServicioPrincipal")
public class MiServicio implements IServicio{
    @Override
    public String operacion(){
        return "Ejecutando algún proceso importante simple...";
    }
}
